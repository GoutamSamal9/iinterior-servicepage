let slideIndexitem = 1;
showSlidesitem(slideIndexitem);

function plusSlidesitem(n) {
  showSlidesitem((slideIndexitem += n));
}

function currentSlideitem(n) {
  showSlidesitem((slideIndexitem = n));
}

function showSlidesitem(n) {
  let items;
  let slidesItems = document.getElementsByClassName("demoimgSlides");
  let controleItem = document.getElementsByClassName("types_of");
  if (n > slidesItems.length) {
    slideIndexitem = 1;
  }
  if (n < 1) {
    slideIndexitem = slidesItems.length;
  }
  for (items = 0; items < slidesItems.length; items++) {
    slidesItems[items].style.display = "none";
  }
  for (items = 0; items < controleItem.length; items++) {
    controleItem[items].className = controleItem[items].className.replace(
      " active",
      "",
    );
  }
  slidesItems[slideIndexitem - 1].style.display = "block";
  controleItem[slideIndexitem - 1].className += " active";
}

// services details2

let slideIndexitemDemo = 1;
showSlidesitemFunction(slideIndexitemDemo);

function plusSlidesitem(n) {
  showSlidesitemFunction((slideIndexitemDemo += n));
}

function currentSlideitemCount(n) {
  showSlidesitemFunction((slideIndexitemDemo = n));
}

function showSlidesitemFunction(n) {
  let items;
  let slidesItems = document.getElementsByClassName("demoSlides");
  let controleItem = document.getElementsByClassName("clickHandler");
  if (n > slidesItems.length) {
    slideIndexitemDemo = 1;
  }
  if (n < 1) {
    slideIndexitemDemo = slidesItems.length;
  }
  for (items = 0; items < slidesItems.length; items++) {
    slidesItems[items].style.display = "none";
  }
  for (items = 0; items < controleItem.length; items++) {
    controleItem[items].className = controleItem[items].className.replace(
      " active",
      "",
    );
  }
  slidesItems[slideIndexitemDemo - 1].style.display = "block";
  controleItem[slideIndexitemDemo - 1].className += " active";
}

// services details3

let slideIndexitemDemoDt = 1;
showSlidesitemFunctionDt(slideIndexitemDemoDt);

function plusSlidesitem(n) {
  showSlidesitemFunctionDt((slideIndexitemDemoDt += n));
}

function currentSlideitemDt(n) {
  showSlidesitemFunctionDt((slideIndexitemDemoDt = n));
}

function showSlidesitemFunctionDt(n) {
  let items;
  let slidesItems = document.getElementsByClassName("demoimgSlidesDt");
  let controleItem = document.getElementsByClassName("clickHandler");
  if (n > slidesItems.length) {
    slideIndexitemDemoDt = 1;
  }
  if (n < 1) {
    slideIndexitemDemoDt = slidesItems.length;
  }
  for (items = 0; items < slidesItems.length; items++) {
    slidesItems[items].style.display = "none";
  }
  for (items = 0; items < controleItem.length; items++) {
    controleItem[items].className = controleItem[items].className.replace(
      " active",
      "",
    );
  }
  slidesItems[slideIndexitemDemoDt - 1].style.display = "block";
  controleItem[slideIndexitemDemoDt - 1].className += " active";
}

// services details4

let slideIndexitemDemoDm = 1;
showSlidesitemFunctionDm(slideIndexitemDemoDm);

function plusSlidesitem(n) {
  showSlidesitemFunctionDm((slideIndexitemDemoDm += n));
}

function currentSlideitemDm(n) {
  showSlidesitemFunctionDm((slideIndexitemDemoDm = n));
}

function showSlidesitemFunctionDm(n) {
  let items;
  let slidesItems = document.getElementsByClassName("demoimgSlidesDm");
  let controleItem = document.getElementsByClassName("clickHandler");
  if (n > slidesItems.length) {
    slideIndexitemDemoDm = 1;
  }
  if (n < 1) {
    slideIndexitemDemoDm = slidesItems.length;
  }
  for (items = 0; items < slidesItems.length; items++) {
    slidesItems[items].style.display = "none";
  }
  for (items = 0; items < controleItem.length; items++) {
    controleItem[items].className = controleItem[items].className.replace(
      " active",
      "",
    );
  }
  slidesItems[slideIndexitemDemoDm - 1].style.display = "block";
  controleItem[slideIndexitemDemoDm - 1].className += " active";
}

// services details5
// let slideIndexitemFirst = 1;
// showSlidesitemFunctionFirst(slideIndexitemFirst);

// function plusSlidesitem(n) {
//   showSlidesitemFunctionFirst((slideIndexitemFirst += n));
// }

// function currentSlideitemFirst(n) {
//   showSlidesitemFunctionFirst((slideIndexitemFirst = n));
// }

// function showSlidesitemFunctionFirst(n) {
//   let items;
//   let slidesItems = document.getElementsByClassName("demoSlidesFirst");
//   let controleItem = document.getElementsByClassName("clickHandlerFirst");
//   if (n > slidesItems.length) {
//     slideIndexitemFirst = 1;
//   }
//   if (n < 1) {
//     slideIndexitemFirst = slidesItems.length;
//   }
//   for (items = 0; items < slidesItems.length; items++) {
//     slidesItems[items].style.display = "none";
//   }
//   for (items = 0; items < controleItem.length; items++) {
//     controleItem[items].className = controleItem[items].className.replace(
//       "active",
//       "",
//     );
//   }
//   slidesItems[slideIndexitemFirst - 1].style.display = "block";
//   controleItem[slideIndexitemFirst - 1].className += "active";
// }

// services details6
