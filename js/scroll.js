const sr = ScrollReveal({
  duration: 1500,
  reset: true,
});

sr.reveal("h1", {});
sr.reveal("h2", {});
sr.reveal(".btn-solid-lg", {});
sr.reveal(".team_card", {});
sr.reveal(".icon_text", {});
sr.reveal(".fa-stack", {});
sr.reveal(".lightbox-basic", {});
sr.reveal(".thumb", {});
sr.reveal(".list-unstyled", {});
sr.reveal(".basic-1 .image-container", {});
